﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
   public  class EntidadesReserva
    {
        public int IdReserva { get; set; }
        public string FechaEntrada { get; set; }
        public string FechaSalida { get; set; }
        public int FkIdhotel { get; set; }
        public int Fkparticipante { get; set; }

        public EntidadesReserva(int idreserva,string fehcaentrada,string fechasalida,int fkhotel,int fkparticipante)
        {
            IdReserva = idreserva;
            FechaEntrada = fehcaentrada;
            FechaSalida = fechasalida;
            FkIdhotel = fkhotel;
            Fkparticipante = fkparticipante;


        }

    }
}
