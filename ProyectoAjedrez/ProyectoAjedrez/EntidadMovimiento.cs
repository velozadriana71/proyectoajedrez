﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
    public class EntidadMovimiento
    {
        public int IdPartida { get; set; }
        public int IdMovimiento { get; set; }
        public string   Movimiento { get; set; }
        public string Comentario { get; set; }
        public EntidadMovimiento(int idpartida,int idmovimiento,string movimento , string comentario)
        {
            IdPartida = idpartida;
            IdMovimiento = idmovimiento;
            Movimiento = movimento;
            Comentario = comentario;

        }

    }
}
