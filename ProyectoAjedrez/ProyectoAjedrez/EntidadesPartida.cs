﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoAjedrez
{
    public class EntidadesPartida
    {
        public int Idpartida { get; set; }
        public string JornadaFec { get; set; }
        public string   JornadaHora { get; set; }
        public int FkArbitro { get; set; }
        public int FkIdhotel { get; set; }
        public int FkidSala { get; set; }
        public int Entradas { get; set; }

        public EntidadesPartida(int inpartida,string jornadafec,string jornadahora,int fkarbitro,int fkhotel,int fkidsala,int entradas)
        {
            Idpartida = inpartida;
            JornadaFec = jornadafec;
            JornadaHora = jornadahora;
            FkArbitro = fkarbitro;
            FkIdhotel = fkhotel;
            FkidSala = fkidsala;
            Entradas = entradas;
        }

    }
}
