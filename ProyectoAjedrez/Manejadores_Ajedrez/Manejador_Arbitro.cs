﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Arbitro
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadArbitro arbitro)
        {
            return ca.Comando(string.Format("INSERT INTO Arbitro VALUES(" +
           "{0})", arbitro.IdArbitro));

        }
        public string EliminarDatos(EntidadArbitro arbitro)
        {
            return ca.Comando(string.Format("delete from Arbitro where idArbitro='{0}'", arbitro.IdArbitro));
        }
    }
}
