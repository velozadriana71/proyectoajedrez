﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Partida
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesPartida partida)
        {
            return ca.Comando(string.Format("INSERT INTO Partida VALUES(" +
           "'{0}','{1}','{2}','{3}','{4}','{5}','{6}')", partida.Idpartida, partida.JornadaFec, partida.JornadaHora,partida.FkArbitro, partida.FkIdhotel, partida.FkidSala, partida.Entradas));
        }
        public string Modificar(EntidadesPartida partida)
        {
            return ca.Comando(string.Format("UPDATE Partida SET jornadaFec='{0}', " + "jornadahora='{1}', fkidArbitro='{2}', fkidJotel='{3}', fkidSala='{4}' entradas='{5}'where idPartida='{6}'", partida.JornadaFec, partida.JornadaHora, partida.FkArbitro, partida.FkIdhotel, partida.FkidSala, partida.Entradas, partida.Idpartida));
        }
        public string EliminarDatos(EntidadesPartida partida)
        {

            return ca.Comando(string.Format("delete from Partida where idPartida='{0}'", partida.Idpartida));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarArbitro(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idArbitro";
        }
        public void LlenarHotel(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idHotel";
        }
        public void LlenarSala(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idSala";
        }
    }
}
