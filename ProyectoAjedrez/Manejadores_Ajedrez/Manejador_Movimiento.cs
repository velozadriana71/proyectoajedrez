﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Movimiento
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadMovimiento mov)
        {
            return ca.Comando(string.Format("INSERT INTO Movimientos VALUES(" +
           "'{0}','{1}','{2}','{3}')", mov.IdPartida, mov.IdMovimiento, mov.Movimiento, mov.Comentario));
        }
        public string Modificar(EntidadMovimiento mov)
        {
            return ca.Comando(string.Format("UPDATE movimiento SET movimiento='{0}', " + "coomentario='{1}' where idMovimiento='{2}'", mov.Movimiento, mov.Comentario, mov.IdMovimiento));
        }
        public string EliminarDatos(EntidadMovimiento mov)
        {

            return ca.Comando(string.Format("delete from Alumno where idCampeonato='{0}'", mov.IdMovimiento));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarPartida(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idPartida";
        }
    }
}
