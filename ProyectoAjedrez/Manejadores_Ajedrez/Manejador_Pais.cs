﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AccesoDatos;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Pais
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesPais pais)
        {
            return ca.Comando(string.Format("INSERT INTO Pais VALUES(" +
           "'{0}','{1}','{2}','{3}')", pais.Idpais, pais.Nombre, pais.NumerodeClub, pais.Representa));
        }
        public string Modificar(EntidadesPais pais)
        {
            return ca.Comando(string.Format("UPDATE Pais SET nombre='{0}', " + "num_clubs='{1}', representa='{2}' where idpais='{3}'", pais.Nombre, pais.NumerodeClub, pais.Representa, pais.Idpais));
        }
        public string EliminarDatos(EntidadesPais pais)
        {

            return ca.Comando(string.Format("delete from Pais where idPais='{0}'", pais.Idpais));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        
    }
}
