﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using ProyectoAjedrez;


namespace Manejadores_Ajedrez
{

    public class Manejador_Campeonato
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesCampeonato camp)
        {
            return ca.Comando(string.Format("INSERT INTO Campeonato VALUES(" +
           "'{0}','{1}','{2}','{3}')", camp.IdCampeonato, camp.Nombre, camp.Tipo, camp.Fkidparticipante));
        }
        public string Modificar(EntidadesCampeonato camp)
        {
            return ca.Comando(string.Format("UPDATE Campeonato SET nombre='{0}', " + "tipo='{1}', fkparticipante='{2}' where idCampeonato='{3}'", camp.Nombre, camp.Tipo, camp.Fkidparticipante, camp.IdCampeonato));
        }
        public string EliminarDatos(EntidadesCampeonato camp)
        {

            return ca.Comando(string.Format("delete from Alumno where idCampeonato='{0}'", camp.IdCampeonato));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarAlumnos(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "num_Socio";
        }
    }
}
