﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using ProyectoAjedrez;

namespace Manejadores_Ajedrez
{
    public class Manejador_Reserva
    {
        ConexionAjedrez ca = new ConexionAjedrez();
        public string GuardarDatos(EntidadesReserva reserva)
        {
            return ca.Comando(string.Format("INSERT INTO Reserva VALUES(" +
           "'{0}','{1}','{2}','{3}')", reserva.IdReserva, reserva.FechaEntrada, reserva.FechaSalida, reserva.FkIdhotel, reserva.Fkparticipante));
        }
        public string Modificar(EntidadesReserva reserva)
        {
            return ca.Comando(string.Format("UPDATE Reserva SET fechaEntrada='{0}', " + "fechaSalida='{1}', fkidHotel='{2}', fkidparticipante='{2}' where idReserva='{3}'", reserva.FechaEntrada, reserva.FechaSalida, reserva.FkIdhotel, reserva.Fkparticipante, reserva.IdReserva));
        }
        public string EliminarDatos(EntidadesReserva reserva)
        {

            return ca.Comando(string.Format("delete from Alumno where idCampeonato='{0}'", reserva.IdReserva));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }
        public void LlenarReserva(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idReserva";
        }
        public void LlenarParticipante(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "idParticipante";
        }
    }
}
