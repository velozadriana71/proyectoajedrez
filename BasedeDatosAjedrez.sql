create database ajedrez1;
use ajedrez1;

CREATE TABLE Pais (
  idPais INT PRIMARY KEY AUTO_INCREMENT,
  nombre VARCHAR(100),
  num_clubs VARCHAR(100),
  representa VARCHAR(100));
  
CREATE TABLE Participantes(
num_Socio int Primary Key ,
nombre VARCHAR(100),
direccion VARCHAR(100),
fkidpais int,
FOREIGN KEY (fkidpais) REFERENCES Pais (idPais));

CREATE TABLE Arbitro (
idArbitro INT PRIMARY KEY not null,
FOREIGN KEY (idArbitro) REFERENCES Participantes (num_Socio)
);

CREATE TABLE Jugador (
 idJugador int PRIMARY KEY not null,
 nivel VARCHAR(50),
FOREIGN KEY (idJugador) REFERENCES Participantes (num_Socio)  
 );
 
CREATE TABLE Hotel (
idHotel int PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR (100),
direccion VARCHAR (100),
telefono VARCHAR (100));


CREATE TABLE Campeonato(
idCampeonato int PRIMARY KEY AUTO_INCREMENT,
nombre VARCHAR(100),
tipo VARCHAR(100),
fkidParticipante int,
FOREIGN KEY (fkidParticipante) REFERENCES Participantes (num_Socio));



CREATE TABLE Sala (
idHotel int,
idSala int PRIMARY KEY AUTO_INCREMENT,
capacidad int,
medio VARCHAR (100),
FOREIGN KEY (idHotel) REFERENCES Hotel (idHotel));

CREATE TABLE Partida (
idPartida int PRIMARY KEY AUTO_INCREMENT,
jornadaFec date,
jornadahora varchar(100),
fkidArbitro int,
fkidHotel int,
fkidSala int,
entradas int,
FOREIGN KEY (fkidArbitro) REFERENCES Arbitro (idArbitro),
FOREIGN KEY (fkidHotel) REFERENCES Hotel (idHotel),
FOREIGN KEY (fkidSala) REFERENCES Sala (idSala));

CREATE TABLE Juego (
idPartida int,
idJugador int,
color VARCHAR(100),
FOREIGN KEY (idPartida) REFERENCES Partida (idPartida),
FOREIGN KEY (idJugador) REFERENCES Jugador (idJugador));

CREATE TABLE Movimientos (
idPartida int,
idMovimiento int PRIMARY KEY AUTO_INCREMENT,
movimiento Varchar(100),
comentario Varchar (100),
FOREIGN KEY (idPartida) REFERENCES Partida (idPartida));

CREATE TABLE Reserva (
idReserva int PRIMARY KEY AUTO_INCREMENT,
fechaEntrada DATE,
fechaSalida Date,
fkidHotel int,
fkidParticipante int,
FOREIGN KEY (fkidHotel) REFERENCES Hotel (idHotel),
FOREIGN KEY (fkidParticipante) REFERENCES Participantes (num_Socio));

insert into  participantes (
   num_Socio
  ,nombre
  ,direccion
  ,fkidpais
) VALUES (
   123 -- num_Socio - IN int(11)
  ,'ubaldo'  -- nombre - IN varchar(100)
  ,'jose ana'  -- direccion - IN varchar(100)
  ,1   -- fkidpais - IN int(11)
);
insert into pais (
   idPais
  ,nombre
  ,num_clubs
  ,representa
) VALUES (
   1 -- idPais - IN int(11)
  ,'mexico'  -- nombre - IN varchar(100)
  ,'9'  -- num_clubs - IN varchar(100)
  ,'Mexico'  -- representa - IN varchar(100)
);
insert into participantes (
   num_Socio
  ,nombre
  ,direccion
  ,fkidpais
) VALUES (
   102 -- num_Socio - IN int(11)
  ,'ubaldo'  -- nombre - IN varchar(100)
  ,'tepe'  -- direccion - IN varchar(100)
  ,1   -- fkidpais - IN int(11)
);
insert into arbitro (
   idArbitro
) VALUES (
   101 -- idArbitro - IN int(11)
);
insert into jugador (
   idJugador
  ,nivel
) VALUES (
   102 -- idJugador - IN int(11)
  ,'Avanzado'  -- nivel - IN varchar(50)
);
insert into hotel (
   idHotel
  ,nombre
  ,direccion
  ,telefono
) VALUES (
   0001 -- idHotel - IN int(11)
  ,'spa flores'  -- nombre - IN varchar(100)
  ,'lagos'  -- direccion - IN varchar(100)
  ,'474741020472'  -- telefono - IN varchar(100)
);
insert into  sala (
   idHotel
  ,idSala
  ,capacidad
  ,medio
) VALUES (
   1   -- idHotel - IN int(11)
  ,1 -- idSala - IN int(11)
  ,200   -- capacidad - IN int(11)
  ,'mediodeque'  -- medio - IN varchar(100)
);
insert into hotel (
   idHotel
  ,nombre
  ,direccion
  ,telefono
) VALUES (
   NULL -- idHotel - IN int(11)
  ,'Univa'  -- nombre - IN varchar(100)
  ,'C.jose'  -- direccion - IN varchar(100)
  ,'892333'  -- telefono - IN varchar(100)
);
insert into campeonato (
   idCampeonato
  ,nombre
  ,tipo
  ,fkidParticipante
) VALUES (
   NULL -- idCampeonato - IN int(11)
  ,'Ubaldo'  -- nombre - IN varchar(100)
  ,'jugador'  -- tipo - IN varchar(100)
  ,102   -- fkidParticipante - IN int(11)
);
insert into  campeonato (
   idCampeonato
  ,nombre
  ,tipo
  ,fkidParticipante
) VALUES (
   NULL -- idCampeonato - IN int(11)
  ,'angel'  -- nombre - IN varchar(100)
  ,'albitro'  -- tipo - IN varchar(100)
  ,101   -- fkidParticipante - IN int(11)
);
insert into partida (
   idPartida
  ,jornadaFec
  ,jornadahora
  ,fkidArbitro
  ,fkidHotel
  ,fkidSala
  ,entradas
) VALUES (
   NULL -- idPartida - IN int(11)
  ,'2020-12-02'  -- jornadaFec - IN date
  ,'11:20'  -- jornadahora - IN varchar(100)
  ,101   -- fkidArbitro - IN int(11)
  ,1  -- fkidHotel - IN int(11)
  ,1   -- fkidSala - IN int(11)
  ,12   -- entradas - IN int(11)
);
insert into juego (
   idPartida
  ,idJugador
  ,color
) VALUES (
   1   -- idPartida - IN int(11)
  ,102   -- idJugador - IN int(11)
  ,'Blanco'  -- color - IN varchar(100)
);
insert into movimientos (
   idPartida
  ,idMovimiento
  ,movimiento
  ,comentario
) VALUES (
   1   -- idPartida - IN int(11)
  ,NULL -- idMovimiento - IN int(11)
  ,'de un lado a otro'  -- movimiento - IN varchar(100)
  ,'se tardo 30 segundos y se caso un moco'  -- comentario - IN varchar(100)
);
insert into reserva (
   idReserva
  ,fechaEntrada
  ,fechaSalida
  ,fkidHotel
  ,fkidParticipante
) VALUES (
   NULL -- idReserva - IN int(11)
  ,'2020-12-01'  -- fechaEntrada - IN date
  ,'2020-12-04'  -- fechaSalida - IN date
  ,1   -- fkidHotel - IN int(11)
  ,101   -- fkidParticipante - IN int(11)
);